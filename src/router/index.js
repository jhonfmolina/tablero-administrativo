import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/servicios",
    name: "servicios",
    component: ()=> import("../components/inicio/Servicios.vue")
  },
  {
    path: "/nosotros",
    name: "nosotros",
    component: ()=> import("../components/inicio/Nosotros.vue")
  },
  {
    path: "/blog",
    name: "blog",
    component: ()=> import("../components/inicio/Blog.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
